[![Docker Stars](https://img.shields.io/docker/stars/capitalone/hygieia-ui.svg)](https://hub.docker.com/r/capitalone/hygieia-api/)
[![Docker Stars](https://img.shields.io/docker/pulls/capitalone/hygieia-ui.svg)](https://hub.docker.com/r/capitalone/hygieia-api/)

## DevOps Quality Engineering Dashboard

### Requirements

- NodeJS
- npm
- gulp
- bower

#### Mac OS X

    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    brew install node
    npm install -g bower
    npm install -g gulp

Pull down everything that's configured with bower and npm.

    npm install
    bower install

Will need up update the ngFitText bower.json file to point to 'src/ng-FitText.js' instead of '/src/ng-FitText.js'

#### Windows

Install NodeJS using the MSI package available at: http://nodejs.org/download/

Issue the following commands via command line:

	npm install -g bower
	npm install -g gulp

Navigate to your project root via command line and use the following command:

	npm install

Use Git Shell to install bower in the following manner; do so from your project's root directory:

	bower install
	select option 2 when prompted for user input

Run the dashboard from the following command:

	gulp serve



```bash
 gulp serve --local true
```

Using browser-sync's [`ghostMode`](https://www.browsersync.io/docs/options#option-ghostMode) functionality:
```bash
gulp serve:ghost-mode
```
